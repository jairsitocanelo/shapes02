abstract public class shapes {

    protected int size;
  
    abstract public void draw();
    abstract public float area();
    abstract public float circumference();
  
    public int getSize() { return size; }
    public void setSize(int size) { this.size = size; }
    public int getWidth(int size) {return size;}
    public int getHeight(int size) {return size;}
  
    public shapes(){
      this.size = 3;
      this.size = 4;
    }
  
    public shapes(int size){
      this.size = size;
    }
    public static void main(String[] args){
      System.out.println();
    }
  }